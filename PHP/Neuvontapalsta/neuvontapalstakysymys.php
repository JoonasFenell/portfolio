<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="neuvontapalsta.css">
		<title>Neuvontapalsta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div class="container">
			<nav>
				<ul>
					<li><a href="neuvontapalsta.php">Etusivu</a></li>
					<li id="drop-nav"><a href="#">Kategoriat</a>
						<ul>
							<li><a href="neuvontapalsta.php?kategoria=tietokoneet">Tietokoneet</a></li>
							<li><a href="neuvontapalsta.php?kategoria=ruoka">Ruoka ja Juomat</a></li>
							<li><a href="neuvontapalsta.php?kategoria=elaimet">Eläimet</a></li>
						</ul>
					</li>
					<?php 
						if(!isset($_SESSION["nimi"])){
							echo '<li><a href="kirjaudu.php">Kirjaudu sisään</a></li>';
						}
					?>
					<?php
						if(!isset($_SESSION["nimi"])){
							echo '<li><a href="rekisterointi.php">Rekisteröidy</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="kysy.php">Kysy</a></li>';
						}
					?>
				</ul>
			</nav>
  
		<header>
			<h1>NEUVONTAPALSTA</h1>
		</header>
		<div id="content">
			<div id="spacer">
			</div>
		<?php 
			require("funktiot.php");
			$yhteys = yhdista_tietokantaan();
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					$vastaus = $_POST['vastaus'];
					$kayttaja = $_SESSION["k_id"]; 
					$kysymys_id= $_GET["kysymys"];
					$query = "INSERT into `vastaus` (pvm, vastaus_sisalto, kayttaja_id, kysymys_id) VALUES (now(), '$vastaus', $kayttaja , $kysymys_id)";
					$result = mysql_query($query);
					if($result){
						echo '<h3 style="margin-left:100px;">Vastaus lähetetty.</h3><br/>';
					}else{
						exit("Tietokantahaku epäonnistui: " . mysql_error());
					}
				}
			if(isset($_GET["kysymys"])){
				
				$vastaa = mysql_real_escape_string($_GET["kysymys"]);
				$sql = "select * from kysymys, kayttaja where kayttaja.kayttaja_id=kysymys.kayttaja_id AND kysymys.kysymys_id=$vastaa";
				$tulos = mysql_query($sql, $yhteys);
				
				if(!$tulos)
					exit("Tietokantahaku epäonnistui: " . mysql_error());
				
				while($kysymys = mysql_fetch_assoc($tulos)){
					echo '<div id="section">
					<div id="otsikko"><a href="#">'.$kysymys ["otsikko"].'</a></div>';
					echo $kysymys['nimi'].', '.$kysymys["kysymys_pvm"].'<br><br>';
					echo '<p>'.$kysymys["kysymys_sisalto"].'</p></div>';
				}
				
				$vastaussql = "select * from vastaus, kayttaja where kayttaja.kayttaja_id=vastaus.kayttaja_id AND vastaus.kysymys_id=$vastaa";
				$vastaustulos = mysql_query($vastaussql, $yhteys);
				
				if(!$vastaustulos)
					exit("Tietokantahaku epäonnistui: " . mysql_error());

				while($vastaus = mysql_fetch_assoc($vastaustulos)){
					echo '<div id="section">';
					echo $vastaus['nimi'].', '.$vastaus["pvm"].'<br><br>';
					echo '<p>'.$vastaus["vastaus_sisalto"].'</p></div>';
				}
					
				echo '<div class="form">';
				echo '<form name="vastauslomake" action="neuvontapalstakysymys.php?kysymys='.$vastaa.'" METHOD="POST">';
				echo '<textarea rows="4" cols="50" name="vastaus" placeholder="Kirjoita vastaus tähän." required autofocus></textarea>';
				if(isset($_SESSION["nimi"])){
					echo '<br><input type="submit" value="vastaa">';
					echo '</form></div>';
				}
			}
		?>
		<footer>
		</footer>
		</div>
	</div>
</body>
</html>