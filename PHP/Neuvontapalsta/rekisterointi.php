<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="neuvontapalsta.css">
		<title>neuvontapalsta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div class="container">
			<nav>
				<ul>
					<li><a href="neuvontapalsta.php">Etusivu</a></li>
					<li id="drop-nav"><a href="#">Kategoriat</a>
						<ul>
							<li><a href="neuvontapalsta.php?kategoria=tietokoneet">Tietokoneet</a></li>
							<li><a href="neuvontapalsta.php?kategoria=ruoka">Ruoka ja Juomat</a></li>
							<li><a href="neuvontapalsta.php?kategoria=elaimet">Eläimet</a></li>
						</ul>
					</li>
					<?php 
						if(!isset($_SESSION["nimi"])){
							echo '<li><a href="kirjaudu.php">Kirjaudu sisään</a></li>';
						}
					?>
					<?php
						if(!isset($_SESSION["nimi"])){
							echo '<li><a href="rekisterointi.php">Rekisteröidy</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="kysy.php">Kysy</a></li>';
						}
					?>
				</ul>
			</nav>
		<header>
			<h1>NEUVONTAPALSTA</h1>
		</header>
		<div id="content">
			<div id="spacer">
			</div>
			<?php
				require("funktiot.php");
				
				$yhteys = yhdista_tietokantaan();
				$nimi = $email = $pw = "";
				
					if ($_SERVER["REQUEST_METHOD"] == "POST") {
						
						$nimi = $_POST['nimi'];
						$email = $_POST['email'];
						$pw = $_POST['pw'];
						$query = "INSERT into `kayttaja` (nimi, pw, email) VALUES ('$nimi', '".md5($pw)."', '$email')";
						$result = mysql_query($query);
						
						if($result){
							echo "<div class='form'><h3>Rekisteröinti onnistui.</h3><br/>Paina täältä kirjautuaksesi sisään<a href='kirjaudu.php'>Kirjaudu</a></div>";
						}else{
							exit("Tietokantahaku epäonnistui: " . mysql_error());
						}
 
					}else{
			?>
				<div class="form">
					<h2>Rekisteröinti</h2>
					<form name="rekisterointi" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
						<input type="text" name="nimi" placeholder="Nimi" required />
						<input type="email" name="email" placeholder="Sähköposti" required />
						<input type="password" name="pw" placeholder="Salasana" required />
						<input type="submit" name="submit" value="Rekisteröidy" />
					</form>
				</div>
				<?php } ?>
			</div>
		</div>
		<footer>
		</footer>
	</body>
</html>