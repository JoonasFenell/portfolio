<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="neuvontapalsta.css">
		<title>Neuvontapalsta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div class="container">
			<nav>
				<ul>
					<li><a href="neuvontapalsta.php">Etusivu</a></li>
					<li id="drop-nav"><a href="#">Kategoriat</a>
						<ul>
							<li><a href="neuvontapalsta.php?kategoria=tietokoneet">Tietokoneet</a></li>
							<li><a href="neuvontapalsta.php?kategoria=ruoka">Ruoka ja Juomat</a></li>
							<li><a href="neuvontapalsta.php?kategoria=elaimet">Eläimet</a></li>
						</ul>
					</li>
					<?php 
						if(!isset($_SESSION["nimi"])){
						echo '<li><a href="kirjaudu.php">Kirjaudu sisään</a></li>';
						}
					?>
					<?php
						if(!isset($_SESSION["nimi"])){
							echo '<li><a href="rekisterointi.php">Rekisteröidy</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="kysy.php">Kysy</a></li>';
						}
					?>
				</ul>
			</nav>
		<header>
			<h1>NEUVONTAPALSTA</h1>
		</header>
		<div id="content">
			<div id="spacer">
			</div>
		<?php 
			require("funktiot.php");
			if(isset($_GET["kategoria"])){

				$yhteys = yhdista_tietokantaan();
				$kategoria = mysql_real_escape_string($_GET["kategoria"]);
				$sql = "select * from kysymys,kayttaja where kysymys.kayttaja_id=kayttaja.kayttaja_id AND kysymys.kategoria='$kategoria'";
				$tulos = mysql_query($sql, $yhteys);

				if(!$tulos)
					exit("Tietokantahaku epäonnistui: " . mysql_error());
		
				while($kysymys = mysql_fetch_assoc($tulos)){
					echo '<div id="section">
					<div id="otsikko"><a href="neuvontapalstakysymys.php?kysymys='.$kysymys["kysymys_id"].'">'.$kysymys ["otsikko"].'</a></div>';
					echo $kysymys['nimi'].', '.$kysymys["kysymys_pvm"].'<br><br>';
					echo '<p>'.$kysymys["kysymys_sisalto"].'</p>';
						if(isset($_SESSION["nimi"])){
							echo '<a href="neuvontapalstakysymys.php?kysymys='.$kysymys["kysymys_id"].'" class="button">Vastaa</a><br>';

						}
					echo '</div>';
  
				}  
			}else{

				$yhteys = yhdista_tietokantaan();
				$sql = "select * from kayttaja,kysymys WHERE kysymys.kayttaja_id=kayttaja.kayttaja_id ORDER BY kysymys_pvm desc";
				$tulos = mysql_query($sql, $yhteys);
				
				if(!$tulos)
					exit("Tietokantahaku epäonnistui: " . mysql_error());

				while($kysymys = mysql_fetch_assoc($tulos)){
					echo '<div id="section">
					<div id="otsikko"><a href="neuvontapalstakysymys.php?kysymys='.$kysymys["kysymys_id"].'">'.$kysymys ["otsikko"].'</a></div>';
					echo $kysymys['nimi'].', '.$kysymys["kysymys_pvm"].'<br><br>';
					echo '<p>'.$kysymys["kysymys_sisalto"].'</p>';
						if(isset($_SESSION["nimi"])){
							echo '<a href="neuvontapalstakysymys.php?kysymys='.$kysymys["kysymys_id"].'" class="button">Vastaa</a><br>';

						}
					echo '</div>';
  
				}
			} 
		?>

		</div>
		<footer>
		</footer>
		</div>
	</body>
</html>