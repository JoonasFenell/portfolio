<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="neuvontapalsta.css">
		<title>neuvontapalsta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div class="container">
			<nav>
				<ul>
					<li><a href="neuvontapalsta.php">Etusivu</a></li>
					<li id="drop-nav"><a href="#">Kategoriat</a>
						<ul>
							<li><a href="neuvontapalsta.php?kategoria=tietokoneet">Tietokoneet</a></li>
							<li><a href="neuvontapalsta.php?kategoria=ruoka">Ruoka ja Juomat</a></li>
							<li><a href="neuvontapalsta.php?kategoria=elaimet">Eläimet</a></li>
						</ul>
					</li>
					<?php 
						if(!isset($_SESSION["nimi"])){
							echo '<li><a href="kirjaudu.php">Kirjaudu sisään</a></li>';
						}
					?>
					<?php
						if(!isset($_SESSION["nimi"])){
							echo '<li><a href="rekisterointi.php">Rekisteröidy</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
						}
					?>
					<?php
						if(isset($_SESSION["nimi"])){
							echo '<li><a href="kysy.php">Kysy</a></li>';
						}
					?>
				</ul>
			</nav>
	<header>
		<h1>NEUVONTAPALSTA</h1>
	</header>
	<div id="content">
		<div id="spacer">
		</div>
		<?php
			require("funktiot.php");
			$yhteys = yhdista_tietokantaan();
			$nimi = $email = $pw = "";
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$otsikko = $_POST['otsikko'];
				$kuvaus = $_POST['kuvaus'];
				$k_id= $_SESSION['k_id'];
				$kategoria = $_POST['kategoria'];
				$timestamp = date('Y-m-d G:i:s');
				$query = "INSERT into `kysymys` (otsikko, kysymys_sisalto, kategoria, kysymys_pvm, kayttaja_id) VALUES ('$otsikko', '$kuvaus', '$kategoria', now(), $k_id)";
				$result = mysql_query($query);
				
				if($result){
					echo "<div class='form'><h3>Kysymyksen lähettäminen onnistui.</h3>";
				}else{
					exit("Tietokantahaku epäonnistui: " . mysql_error());
				}
 
			}else{
		?>
		<div class="form1">
			<h2>Uusi kysymys</h2>
			<form name="rekisterointi" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
				<input type="text" name="otsikko" placeholder="Otsikko" required />
				<textarea name="kuvaus" placeholder="Kysymys" cols="30" rows="5"></textarea>
				<select name="kategoria" style="color:gray">
					<option value="tietokoneet">Tietokoneet</option>
					<option value="ruoka">Ruoka ja Juomat</option>
					<option value="elaimet">Eläimet</option>
				</select>
				<input type="submit" name="submit" value="Lähetä kysymys" />
			</form>
		</div>
		<?php } ?>
		</div>
		<footer>
		</footer>
		</div>
	</body>
</html>