-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2016 at 09:40 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `neuvontapalsta`
--

-- --------------------------------------------------------

--
-- Table structure for table `kayttaja`
--

CREATE TABLE IF NOT EXISTS `kayttaja` (
  `kayttaja_id` int(11) NOT NULL,
  `nimi` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `pw` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kayttaja`
--

INSERT INTO `kayttaja` (`kayttaja_id`, `nimi`, `email`, `pw`) VALUES
(1, 'Joonas', 'joonas.fenell@gmail.com', 'af55da4ceaf316e3d12c11a5e2f8d0a9'),
(2, 'Jari', 'joonas.fenell@gmail.com', '7815696ecbf1c96e6894b779456d330e');

-- --------------------------------------------------------

--
-- Table structure for table `kysymys`
--

CREATE TABLE IF NOT EXISTS `kysymys` (
  `kysymys_id` int(11) NOT NULL,
  `kysymys_sisalto` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `kysymys_pvm` date DEFAULT NULL,
  `aktiivinen` tinyint(1) NOT NULL,
  `kayttaja_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `otsikko` varchar(50) NOT NULL,
  `kategoria` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kysymys`
--

INSERT INTO `kysymys` (`kysymys_id`, `kysymys_sisalto`, `kysymys_pvm`, `aktiivinen`, `kayttaja_id`, `otsikko`, `kategoria`) VALUES
(6, 'Mitä on etninen ruoka?\r\n\r\nOnko olemassa Suomalaista etnistä ruokaa?\r\n\r\nOnko suomalaisen keittion ruoka kansainvälistä ?', '2016-04-17', 0, 1, 'Etninen ruoka', 'ruoka'),
(7, 'Koneeni jäätyy melkeen aina kun sen käynnistää niin alkuvalikkoon josta pääsee boot menuun mutta ei anna tehdä mitään . olen koittanut biossin patteria irroittaa mutta ei auta. Aikaisemmin oli ongelma että näytölle ei ilmaantunut mitään vaikka kone lähti käymään mutta se korjaantui uunittamalla emolevyn. Olisiko uunitus voinut vaurioittaa emolevyä? Ihmetyttää vaan kun välillä hyvin harvoin konetta pääsee ihan normaalisti käyttämään.', '2016-05-17', 0, 2, 'missä vika kun kone ei käynnisty', 'tietokoneet'),
(8, 'Kannettava tietokoneeni on alkanut ilman syytä kohisemaan. Kohina alkaa jonkin ajan kuluttua, kun olen sen avannut ja alkanut joko kirjoittaa tai selaamaan nettiä. Mielestäni se ei kohissut uutena muutamaan vuoteen. Tulee mieleen, että onko kyse siihen ujutetuista haittaohjelmista, jotka elävät omaa elämäänsä ja kuormittavat konetta kohinaa aiheuttaen. Onko siitä kyse?\r\n\r\nVoiko asialle tehdä mitään?', '2016-05-17', 0, 1, 'Kohisemien ärsyttää', 'tietokoneet');

-- --------------------------------------------------------

--
-- Table structure for table `vastaus`
--

CREATE TABLE IF NOT EXISTS `vastaus` (
  `vastaus_sisalto` text,
  `pvm` date DEFAULT NULL,
  `kayttaja_id` int(11) DEFAULT NULL,
  `kysymys_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vastaus`
--

INSERT INTO `vastaus` (`vastaus_sisalto`, `pvm`, `kayttaja_id`, `kysymys_id`) VALUES
('Käyttöjärjestelmän uudelleen asennus edessä. Varmaan jotain pöpöjä koneella.\r\n\r\nVoisit koettaa siinää käynnistyksen alussa painella f8 nappulaa siinä näppäimistön ylälaidassa jos pääset sellaiseeen valikkotilaan missä vois valita viimeinen toimiva rivin ja niilla nuolinäppäimillä menet sille riville ja sittten painat enter; jos se ei auta niin sitten on keinot vähissä.', '2016-05-18', 2, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kayttaja`
--
ALTER TABLE `kayttaja`
  ADD PRIMARY KEY (`kayttaja_id`);

--
-- Indexes for table `kysymys`
--
ALTER TABLE `kysymys`
  ADD PRIMARY KEY (`kysymys_id`);

--
-- Indexes for table `vastaus`
--
ALTER TABLE `vastaus`
  ADD KEY `kayttaja` (`kayttaja_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kayttaja`
--
ALTER TABLE `kayttaja`
  MODIFY `kayttaja_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kysymys`
--
ALTER TABLE `kysymys`
  MODIFY `kysymys_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
