<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="uutissivusto.css">
		<title>Uutissivusto</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet' type='text/css'>
	</head>
	<body background="tk.png" class="bg">
		<div id="container">
			<header>
				<div id="header">
					<h1>Satuvaltakunnan tarinat</h1>
					<h2>Uutisia lumotusta masata<h2>
				</div>
			</header>
			<div id="content">
				<div id="saa">
					<div id="viikonsaa">
						<p> Viikon sää - viikko 21 </p>
					</div>
				<div id="viikonsaa1">
				<?php
				
					$xml=simplexml_load_file("http://localhost/php/saatiedotus.php?vko=21") or die("Virhe: XML-syötteen käsittely epäonnistui");
					$viikonpaiva=0;
					
					foreach($xml->children() as $saa) { 
					$viikonpaiva++;
					echo "<div class='paiva'>";
						if($viikonpaiva==1){
							echo "<font size='4'>Maanantai</font> ";
						}
						if($viikonpaiva==2){
							echo "<font size='4'>Tiistai</font> ";
						}
						if($viikonpaiva==3){
							echo "<font size='4'>keskiviikko</font> ";
						}
						if($viikonpaiva==4){
							echo "<font size='4'>Torstai</font> ";
						}
						if($viikonpaiva==5){
							echo "<font size='4'>Perjantai</font> ";
						}
						if($viikonpaiva==6){
							echo "<font size='4'>Lauantai</font> ";
						}
						if($viikonpaiva==7){
							echo "<font size='4'>Sunnuntai</font> ";
						}
						echo "<br>";
						$date=date_create($saa['paiva']);
						echo date_format($date,"d.m.Y"). "<br>";
						echo $saa->saatila. "<br>";
						echo "<font size='5'> ".$saa->lampotila. "&#8451</font><br>";
						echo "tuulennopeus: ".$saa->tuulennopeus. "m/s<br>";
						echo "</div>";
					}
				?>
				</div><br>
				<div id="uutisetblogi">
				
					<?php
						require("funktiot.php");
						$yhteys = yhdista_tietokantaan();
						$sql = "select otsikko, julkaisuaika, kirjoittaja, sisalto from uutiset ORDER BY julkaisuaika desc
						LIMIT 2";
						$tulos = mysql_query($sql, $yhteys);

						if(!$tulos)
							exit("Tietokantahaku epäonnistui: " . mysql_error());
						while($uutinen = mysql_fetch_assoc($tulos)){
							$date1=date_create($uutinen["julkaisuaika"]);
							echo '<div class="uutiset1">
							'.'<h3>'.$uutinen["otsikko"].'</h3>'.date_format($date1,"d.m.Y H:i").' || '.$uutinen["kirjoittaja"].'<br>';
							echo '<p>'.$uutinen['sisalto'].'</p>';
							echo '</div>';
						}
					?>
				</div>
					<div id="sivu">
						<div class="sivuotsikko">
							Uusimmat uutiset
						</div>
						<?php
						
							$sql1 = "select otsikko, julkaisuaika from uutiset ORDER BY julkaisuaika desc
							LIMIT 5";
							$tulos1 = mysql_query($sql1, $yhteys);
							
							if(!$tulos1)
								exit("Tietokantahaku epäonnistui: " . mysql_error());
							while($uutinen1 = mysql_fetch_assoc($tulos1)){
								$date1=date_create($uutinen1["julkaisuaika"]);
								echo '<div class="sivuuutinen1">
								'.'<h4>'.$uutinen1["otsikko"].'</h4>'.date_format($date1,"d.m.Y H:i");
								echo '</div>';
							}
						?><br>
						<div id="sivuuutinen2">
							<div class="sivuotsikko">
								Vierailevat kirjoittajat
							</div>
						<?php
						
							$string = file_get_contents("http://localhost/php/blogikirjoitus.php");
							$json_a = json_decode($string, true);
						
							foreach ($json_a as $blogi => $blogi_a) {
								echo '<div class="sivuuutinen1">';
								echo "<h4>".$blogi_a['nimi'].": ";
								echo $blogi_a['otsikko']."</h4>";
								echo " ". $blogi_a['kirjoittaja'];
								$date2=date_create($blogi_a['julkaisuaika']);
								echo " ".date_format($date2,"d.m.Y H:i");
								echo '</div>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
